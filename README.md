![alt text](./ERD/ERD.png)

EndPoint Rest API
All Cars :
http://localhost:3000/

Add Cars :
http://localhost:3000/add-car

Filter Cars size small
http://localhost:3000/cars/size/small

Filter Cars size medium
http://localhost:3000/cars/size/medium

Filter Cars size large
http://localhost:3000/cars/size/large

const { Car } = require("../models");
const fs = require("fs");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const axios = require("axios");
const moment = require("moment");
const PORT = process.env.PORT || 3000;

const deleteImage = async (id) => {
  const car = await Car.findOne({
    where: {
      id: id,
    },
  });
  const image = car.image;
  fs.unlink(`./public/uploads/${image}`, (err) => {
    if (err) {
      return err;
    }
  });
};

const createCar = (req, res) => {
  const { name, price, size } = req.body;
  const image = req.file ? req.file.filename : "";
  Car.create({
    name,
    price,
    size,
    image,
  })
    .then(() => {
      res.status(200).json({
        message: "Car created",
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error creating car",
      });
    });
};

const carsBySize = (req, res) => {
  const { size } = req.params;
  Car.findAll({
    where: {
      size: size,
    },
  })
    .then((car) => {
      res.status(200).json(car);
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error getting cars by size",
      });
    });
};

const CarsByKeyword = (req, res) => {
  const { keyword } = req.params;
  Car.findAll({
    where: {
      name: {
        [Op.iLike]: `%${keyword}%`,
      },
    },
  })
    .then((car) => {
      res.status(200).json(car);
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error getting cars by keyword",
      });
    });
};

const listCar = (req, res) => {
  Car.findAll({
    order: [["id", "DESC"]],
  })
    .then((car) => {
      res.status(200).json(car);
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error getting cars",
      });
    });
};

const getCar = (req, res) => {
  Car.findOne({
    where: {
      id: req.params.id,
    },
  }).then((car) => {
    res.status(200).json(car);
  });
};

const updateCar = async (req, res) => {
  if (req.file) await deleteImage(req.params.id);
  const { name, price, size } = req.body;
  const image = req.file ? req.file.filename : req.body.oldImage;
  const query = {
    where: {
      id: req.params.id,
    },
  };

  Car.update(
    {
      name,
      price,
      size,
      image,
    },
    query
  )
    .then(() => {
      res.status(200).json({
        message: "Car updated",
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error updating car",
      });
    });
};

const deleteCar = async (req, res) => {
  await deleteImage(req.params.id);
  Car.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      res.status(200).json({
        message: "Car deleted successfully",
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: "Error deleting car",
      });
    });
};
const listCars = (req, res) => {
  axios.get(`http://localhost:${PORT}/cars`).then((response) => {
    const cars = response.data;
    cars.forEach((car) => {
      car.updatedAt = moment(car.updatedAt).format("DD MMM YYYY, HH:mm");
    });
    res.render("index", {
      cars,
      size: "all",
      page: "List Cars",
    });
  });
};

const addCar = (req, res) => {
  res.render("Form", {
    page: "Add New Car",
    method: "POST",
    id: "",
    size: "",
    sizes: ["small", "medium", "large"],
    action: "addCar()",
  });
};

const editCar = (req, res) => {
  axios.get(`http://localhost:${PORT}/cars/${req.params.id}`).then((response) => {
    const car = response.data;
    res.render("Form", {
      page: "Update Car Information",
      method: "PUT",
      id: req.params.id,
      car,
      size: "",
      sizes: ["small", "medium", "large"],
      action: `editCar(${req.params.id})`,
    });
  });
};

const listCarsFilter = (req, res) => {
  axios.get(`http://localhost:${PORT}/cars/filter/${req.params.size}`).then((response) => {
    const cars = response.data;
    const size = req.params.size;
    cars.forEach((car) => {
      car.updatedAt = moment(car.updatedAt).format("DD MMM YYYY, HH:mm");
    });
    res.render("index", {
      cars,
      size,
      page: `List Cars by Size: ${size[0].toUpperCase() + size.slice(1)}`,
    });
  });
};

const listCarsByKeyword = (req, res) => {
  axios.get(`http://localhost:${PORT}/cars/search/${req.params.keyword}`).then((response) => {
    const cars = response.data;
    const keyword = req.params.keyword;
    cars.forEach((car) => {
      car.updatedAt = moment(car.updatedAt).format("DD MMM YYYY, HH:mm");
    });
    res.render("index", {
      cars,
      keyword,
      size: "",
      page: `List Cars by Keyword: ${keyword}`,
    });
  });
};
module.exports = {
  createCar,
  carsBySize,
  CarsByKeyword,
  listCar,
  getCar,
  updateCar,
  deleteCar,
  listCars,
  addCar,
  editCar,
  listCarsFilter,
  listCarsByKeyword,
};
